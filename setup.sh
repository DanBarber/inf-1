#!/bin/bash

# WordPress Apache, PHP, MySQL setup script

# Variables required for script operation
mysqluser=root
mysqlpass=root

# Update packages & system
sudo apt-get update -y && sudo apt-get upgrade -y

# Install apache
sudo apt-get install apache2 -y

# Install php 7.1 because it has mcrypt built in...
sudo add-apt-repository ppa:ondrej/php -y

sudo apt-get update -y

sudo apt-get install php7.1 php7.1-cli php7.1-common php7.1-json php7.1-opcache php7.1-mysql php7.1-mbstring php7.1-mcrypt php7.1-zip php7.1-fpm -y

# Install MySql Server

sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $mysqlpass"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $mysqlpass"

sudo apt-get -y install mysql-server mysql-client mysql-common mysql-utilities